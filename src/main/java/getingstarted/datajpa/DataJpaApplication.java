package getingstarted.datajpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DataJpaApplication {

	private static final Logger log = LoggerFactory.getLogger(DataJpaApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(DataJpaApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			repository.save(new Customer("Jacek", "Placky"));
			repository.save(new Customer("Marian","Ndalorian"));
			repository.save(new Customer("Joana", "Placky"));

			log.info("Customers found with findAll():");
			for (Customer customer : repository.findAll()){
				log.info(customer.toString());
			}

			log.info("Customer found with findById(1L)");
			log.info(repository.findById(1L).toString());

			log.info("Customer found with findByLastName('Placky')");
			repository.findByLastName("Placky").forEach(x -> log.info(x.toString()));
		};
	}
}
